var app = angular.module('aena', ['ngMaterial', 'ngMessages', 'ngRoute'])
app.config(function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode({
    enabled: false,
    requireBase: true
  });
  $locationProvider.hashPrefix('');
  $routeProvider
  .when('/home', {controller: 'HomeCtrl', templateUrl: '/static/views/home/home.html'})
  .when('/detail', {controller: 'DetailCtrl', templateUrl: '/static/views/detail/detail.html'})
  .otherwise({redirectTo:'/home'});
});

app.service('itemDetail', function() {
	this.item = {};
});
