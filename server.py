from flask import Flask, send_file, request
app = Flask(__name__)

@app.route("/")
def init():
    return send_file("templates/index.html")

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=80, debug=True)
