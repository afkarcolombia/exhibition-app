app.controller('HomeCtrl', function ($scope, itemDetail, $timeout) {
  $scope.DetailView = function(item) {
    itemDetail.item = item;
    window.location.href = '#/detail'
  }
  $scope.loading = true;
  $timeout((function () {
    $scope.loading = false
    $(".contentCards").owlCarousel({
      loop: true,
      autoplay: true,
      smartSpeed: 1000,
      fluidSpeed: true,
      animateIn: true,
      autoplayTimeout: 2000,
      autoplayHoverPause: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        960: {
          items: 4
        },
        1200: {
          items: 5
        }
      }
    })
  }), 1000)
 $scope.fotografia = [
    {
      name: 'Aguila Calva',
      img: '/static/public/img/collection/1.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Afilandose Las Uñas',
      img: '/static/public/img/collection/41.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Altos Vuelos',
      img: '/static/public/img/collection/2.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Amor De Madre 1',
      img: '/static/public/img/collection/42.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Azor',
      img: '/static/public/img/collection/3.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Amor De Madre 2',
      img: '/static/public/img/collection/43.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Bigotudo',
      img: '/static/public/img/collection/4.jpg',
      author: 'Augusto Amor' 
    },
     {
      name: 'Bebe',
      img: '/static/public/img/collection/44.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Cabalo Do Demo',
      img: '/static/public/img/collection/5.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Bolitas De Flamenco',
      img: '/static/public/img/collection/45.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Carraca',
      img: '/static/public/img/collection/6.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Carbonero Comun',
      img: '/static/public/img/collection/46.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Cascabel',
      img: '/static/public/img/collection/7.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Elefante',
      img: '/static/public/img/collection/47.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Collalba Negra',
      img: '/static/public/img/collection/8.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Garceta Comun',
      img: '/static/public/img/collection/48.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Crialo',
      img: '/static/public/img/collection/9.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Guacamayo Papillero',
      img: '/static/public/img/collection/49.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Escaramuzas',
      img: '/static/public/img/collection/10.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Herrerillo',
      img: '/static/public/img/collection/50.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Flamencos En La Niebla',
      img: '/static/public/img/collection/11.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'in Fraganti',
      img: '/static/public/img/collection/51.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Harrier',
      img: '/static/public/img/collection/12.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Lemur',
      img: '/static/public/img/collection/55.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Imperial',
      img: '/static/public/img/collection/13.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'LLorando Por La Falta',
      img: '/static/public/img/collection/53.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'La Caza',
      img: '/static/public/img/collection/14.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Lobo 1',
      img: '/static/public/img/collection/54.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'La Cruz',
      img: '/static/public/img/collection/15.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Lobo 2',
      img: '/static/public/img/collection/55.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'La Parada',
      img: '/static/public/img/collection/16.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Medusas',
      img: '/static/public/img/collection/56.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Luces Y Sombras',
      img: '/static/public/img/collection/17.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Pandita',
      img: '/static/public/img/collection/57.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Mama',
      img: '/static/public/img/collection/18.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Picapinos',
      img: '/static/public/img/collection/58.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Mosca',
      img: '/static/public/img/collection/19.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Tigre',
      img: '/static/public/img/collection/59.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Pechiazul',
      img: '/static/public/img/collection/20.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Zopilote Rey',
      img: '/static/public/img/collection/60.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Pico Picapinos',
      img: '/static/public/img/collection/21.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Alien',
      img: '/static/public/img/collection/61.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Picogordo',
      img: '/static/public/img/collection/22.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Amor De Madre 3',
      img: '/static/public/img/collection/62.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Plumerito',
      img: '/static/public/img/collection/23.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Asomados A La Ventana',
      img: '/static/public/img/collection/63.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Sed',
      img: '/static/public/img/collection/24.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Baile',
      img: '/static/public/img/collection/64.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Simetrias',
      img: '/static/public/img/collection/25.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Bufalo Rojo',
      img: '/static/public/img/collection/65.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Azor',
      img: '/static/public/img/collection/26.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Buitre Leonado',
      img: '/static/public/img/collection/66.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Busardo',
      img: '/static/public/img/collection/27.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Buscando Comida',
      img: '/static/public/img/collection/67.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Carabo',
      img: '/static/public/img/collection/28.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Dalia Roja',
      img: '/static/public/img/collection/68.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Carpintero',
      img: '/static/public/img/collection/29.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'El Grito',
      img: '/static/public/img/collection/69.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Contrastes',
      img: '/static/public/img/collection/30.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Flamenco De Perfil',
      img: '/static/public/img/collection/70.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'El Capuchino',
      img: '/static/public/img/collection/31.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Flamencos Laguna',
      img: '/static/public/img/collection/71.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'El Duende',
      img: '/static/public/img/collection/32.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Gibon',
      img: '/static/public/img/collection/72.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Trebol De Cuatro Hojas',
      img: '/static/public/img/collection/33.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Interioridades',
      img: '/static/public/img/collection/73.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'La Bella Matadora',
      img: '/static/public/img/collection/34.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Leopardito',
      img: '/static/public/img/collection/74.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'La Ceba',
      img: '/static/public/img/collection/35.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Lince',
      img: '/static/public/img/collection/75.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'La Rabieta',
      img: '/static/public/img/collection/36.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Mirada',
      img: '/static/public/img/collection/76.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Linx',
      img: '/static/public/img/collection/37.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Pavo Real Blanco',
      img: '/static/public/img/collection/77.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Natrix',
      img: '/static/public/img/collection/38.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Pensativo',
      img: '/static/public/img/collection/78.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Reflejos',
      img: '/static/public/img/collection/39.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Rana',
      img: '/static/public/img/collection/79.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Un Punto De Fuga',
      img: '/static/public/img/collection/40.jpg',
      author: 'Augusto Amor' 
    },
    {
      name: 'Retrato Buitre Leonado',
      img: '/static/public/img/collection/80.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Gacela Dama',
      img: '/static/public/img/collection/81.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Retrato Leopardo',
      img: '/static/public/img/collection/82.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Ruiseñor Pechiazul',
      img: '/static/public/img/collection/83.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Ternura Ternero',
      img: '/static/public/img/collection/84.jpg',
      author: 'Teresa Palacios' 
    },
    {
      name: 'Timidez',
      img: '/static/public/img/collection/85.jpg',
      author: 'Teresa Palacios' 
    },
  ]
})
