app.controller('DetailCtrl', function($scope, itemDetail, $timeout) {
  $scope.detail = itemDetail.item;
  $scope.collection = "Colección";
  // $scope.collectionName = "Personas, Personajes y Políticos";
  $scope.backHome = function() {
    itemDetail.item = {};
    window.location.href = '#/home'
  }
  let img = new Image();
  img.src = $scope.detail.img
  // console.log($scope.img.width, $scope.img.height)
  if(img.width < img.height) {
    $scope.imgSize = {
      width: '27%',
      height:'27%'
    }
  } 
  $timeout(() => {
    window.location.href = '#/home'
  }, 120000)
})
